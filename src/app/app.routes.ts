import { RouterModule, Routes } from "@angular/router";
import { PcComponent } from "./components/pc/pc.component";
import { XboxComponent } from "./components/xbox/xbox.component";
import { PsComponent } from "./components/ps/ps.component";
import { NintendoComponent } from "./components/nintendo/nintendo.component";
import { Lateral1Component } from "./components/lateral1/lateral1.component";
import { Lateral2Component } from "./components/lateral2/lateral2.component";
import { Lateral3Component } from "./components/lateral3/lateral3.component";
import { Lateral4Component } from "./components/lateral4/lateral4.component";



const Routes1: Routes =[
    {path: 'pc', component: PcComponent},
    {path: 'xbox', component: XboxComponent},
    {path: 'ps', component:PsComponent},
    {path: 'nintendo', component:NintendoComponent},
    {path: 'lateral1', component:Lateral1Component},
    {path: 'lateral2', component:Lateral2Component},
    {path: 'lateral3', component:Lateral3Component},
    {path: 'lateral4', component:Lateral4Component},
    {path: '**', pathMatch:'full', redirectTo:'pc'}
]



export const Routing= RouterModule.forRoot(Routes1)