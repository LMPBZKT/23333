import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lateral2Component } from './lateral2.component';

describe('Lateral2Component', () => {
  let component: Lateral2Component;
  let fixture: ComponentFixture<Lateral2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lateral2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lateral2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
