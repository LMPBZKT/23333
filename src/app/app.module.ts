import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PcComponent } from './components/pc/pc.component';
import { XboxComponent } from './components/xbox/xbox.component';
import { NintendoComponent } from './components/nintendo/nintendo.component';
import { PsComponent } from './components/ps/ps.component';
import { NadvarComponent } from './components/nadvar/nadvar.component';
import { SidevarComponent } from './components/sidevar/sidevar.component';
import { Routing } from './app.routes';
import { Lateral1Component } from './components/lateral1/lateral1.component';
import { Lateral2Component } from './components/lateral2/lateral2.component';
import { Lateral3Component } from './components/lateral3/lateral3.component';
import { Lateral4Component } from './components/lateral4/lateral4.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';




@NgModule({
  declarations: [
    AppComponent,
    PcComponent,
    XboxComponent,
    NintendoComponent,
    PsComponent,
    NadvarComponent,
    SidevarComponent,
    Lateral1Component,
    Lateral2Component,
    Lateral3Component,
    Lateral4Component,
    
  ],
  imports: [
    BrowserModule,
    Routing,
    MatSliderModule,
    BrowserAnimationsModule,
    BrowserAnimationsModule,
    MatVideoModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [],
  bootstrap: [ AppComponent ] 
})
export class AppModule { }
